﻿namespace Common360.Entities.Enums
{
    public enum UserType
    {
        Operator = 1,
        Administrator
    }
}
