﻿namespace Common360.Entities.Enums
{
    public enum AccessLevel
    {
        Administrator,
        Operator
    }
}
