"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./models/user"));
__export(require("./models/role"));
__export(require("./models/district"));
__export(require("./models/province"));
__export(require("./models/municipality"));
__export(require("./models/person"));
__export(require("./models/marital-status"));
__export(require("./models/identification-type"));
__export(require("./models/contact-information"));
__export(require("./models/address"));
__export(require("./models/gender"));
__export(require("./models/contact-type"));
__export(require("./models/access-list"));
__export(require("./models/user-type"));
//# sourceMappingURL=index.js.map