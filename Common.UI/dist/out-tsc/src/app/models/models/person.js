"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var address_1 = require("./address");
var Person = /** @class */ (function () {
    function Person() {
        this.address = new address_1.Address();
        this.contactInformations = [];
    }
    return Person;
}());
exports.Person = Person;
//# sourceMappingURL=person.js.map