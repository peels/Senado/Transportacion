"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AccessLevels;
(function (AccessLevels) {
    AccessLevels[AccessLevels["administrator"] = 0] = "administrator";
    AccessLevels[AccessLevels["operator"] = 1] = "operator";
})(AccessLevels = exports.AccessLevels || (exports.AccessLevels = {}));
//# sourceMappingURL=access-levels.js.map