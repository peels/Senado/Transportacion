"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var UserType;
(function (UserType) {
    UserType[UserType["operator"] = 1] = "operator";
    UserType[UserType["administrator"] = 2] = "administrator";
})(UserType = exports.UserType || (exports.UserType = {}));
//# sourceMappingURL=user-type.js.map