"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ContactType;
(function (ContactType) {
    ContactType[ContactType["Home"] = 0] = "Home";
    ContactType[ContactType["Mobile"] = 1] = "Mobile";
    ContactType[ContactType["Work"] = 2] = "Work";
    ContactType[ContactType["Email"] = 3] = "Email";
})(ContactType = exports.ContactType || (exports.ContactType = {}));
//# sourceMappingURL=contact-type.js.map