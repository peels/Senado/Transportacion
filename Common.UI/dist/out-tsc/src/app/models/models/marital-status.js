"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MaritalStatus;
(function (MaritalStatus) {
    MaritalStatus[MaritalStatus["Single"] = 0] = "Single";
    MaritalStatus[MaritalStatus["Married"] = 1] = "Married";
    MaritalStatus[MaritalStatus["Divorce"] = 2] = "Divorce";
    MaritalStatus[MaritalStatus["FreeUnion"] = 3] = "FreeUnion";
    MaritalStatus[MaritalStatus["Widowed"] = 4] = "Widowed";
})(MaritalStatus = exports.MaritalStatus || (exports.MaritalStatus = {}));
//# sourceMappingURL=marital-status.js.map