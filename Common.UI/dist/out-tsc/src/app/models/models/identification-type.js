"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IdentificationType;
(function (IdentificationType) {
    IdentificationType[IdentificationType["NationalIdentificationNumber"] = 0] = "NationalIdentificationNumber";
    IdentificationType[IdentificationType["Passport"] = 1] = "Passport";
})(IdentificationType = exports.IdentificationType || (exports.IdentificationType = {}));
//# sourceMappingURL=identification-type.js.map