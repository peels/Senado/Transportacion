"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var auth_1 = require("app/core/auth");
var http_1 = require("app/core/http");
var base_component_1 = require("app/core/components/base-component");
var LoginComponent = /** @class */ (function (_super) {
    __extends(LoginComponent, _super);
    function LoginComponent(router, formBuilder, authService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.authService = authService;
        _this.loginButtonTitle = 'Login';
        _this.createForm();
        return _this;
    }
    LoginComponent.prototype.createForm = function () {
        var now = new Date();
        this.formLogin = this.formBuilder.group({
            username: ['', forms_1.Validators.required],
            password: ['', forms_1.Validators.required]
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        if (!this.formLogin.valid) {
            return;
        }
        this.message = null;
        this.loginButtonTitle = 'PleaseWaitMessage';
        this.authService.login(this.formLogin.value)
            .then(function (data) {
            _this.router.navigate(['/']);
            _this.loginButtonTitle = 'Login';
        }).catch(function (error) {
            _this.loginButtonTitle = 'Login';
            console.log(error);
            if (error.status === http_1.HttpStatus.UNAUTHORIZED) {
                _this.message = 'InvalidCredentials';
            }
            else {
                _this.message = 'AnErrorHasOcurred';
            }
        });
    };
    LoginComponent = __decorate([
        core_1.Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router, forms_1.FormBuilder,
            auth_1.AuthService,
            core_1.Injector])
    ], LoginComponent);
    return LoginComponent;
}(base_component_1.BaseComponent));
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map