"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_1 = require("app/core/auth");
var base_component_1 = require("app/core/components/base-component");
var forms_1 = require("@angular/forms");
var ChangePasswordComponent = /** @class */ (function (_super) {
    __extends(ChangePasswordComponent, _super);
    function ChangePasswordComponent(authService, fb, injector) {
        var _this = _super.call(this, injector) || this;
        _this.authService = authService;
        _this.fb = fb;
        _this.loading = true;
        return _this;
    }
    ChangePasswordComponent.prototype.ngOnInit = function () {
        this.form = this.fb.group({
            currentPassword: ['', forms_1.Validators.required],
            newPassword: ['', forms_1.Validators.required],
            confirmPassword: ['', forms_1.Validators.required]
        }, { validator: this.checkIfMatchingPasswords('newPassword', 'confirmPassword') });
        this.currentPassword = this.form.get('currentPassword');
        this.newPassword = this.form.get('newPassword');
        this.confirmPassword = this.form.get('confirmPassword');
        this.loading = false;
    };
    ChangePasswordComponent.prototype.checkIfMatchingPasswords = function (passwordKey, passwordConfirmationKey) {
        return function (group) {
            var passwordInput = group.controls[passwordKey], passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                passwordConfirmationInput.setErrors({ notMach: true });
                passwordConfirmationInput.markAsTouched();
            }
        };
    };
    ChangePasswordComponent.prototype.onSubmit = function () {
        var _this = this;
        if (!this.form.valid) {
            return;
        }
        var value = this.form.value;
        Promise.all([
            this.authService.changePassword(value.currentPassword, value.newPassword),
            this.translateService.get('PasswordChangedMessage').toPromise()
        ]).then(function (data) {
            _this.toastr.success(data[1]);
        });
    };
    ChangePasswordComponent = __decorate([
        core_1.Component({
            selector: 'app-change-password',
            templateUrl: './change-password.component.html',
            styleUrls: ['./change-password.component.scss']
        }),
        __metadata("design:paramtypes", [auth_1.AuthService, forms_1.FormBuilder, core_1.Injector])
    ], ChangePasswordComponent);
    return ChangePasswordComponent;
}(base_component_1.BaseComponent));
exports.ChangePasswordComponent = ChangePasswordComponent;
//# sourceMappingURL=change-password.component.js.map