"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var login_component_1 = require("./login/login.component");
var router_1 = require("@angular/router");
var routes = [
    { path: 'login', component: login_component_1.LoginComponent },
];
exports.AccountRoutes = router_1.RouterModule.forChild(routes);
//# sourceMappingURL=account.routing.js.map