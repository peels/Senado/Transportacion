"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_guard_1 = require("./core/auth/auth.guard");
var auth_service_1 = require("./core/auth/auth.service");
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var common_1 = require("@angular/common");
var app_component_1 = require("./app.component");
var dropdown_1 = require("ngx-bootstrap/dropdown");
var tabs_1 = require("ngx-bootstrap/tabs");
var nav_dropdown_directive_1 = require("./shared/nav-dropdown.directive");
var ng2_charts_1 = require("ng2-charts/ng2-charts");
var sidebar_directive_1 = require("./shared/sidebar.directive");
var aside_directive_1 = require("./shared/aside.directive");
var breadcrumb_component_1 = require("./shared/breadcrumb.component");
// Routing Module
var app_routing_1 = require("./app.routing");
// Layouts
var full_layout_component_1 = require("./layouts/full-layout.component");
var http_1 = require("@angular/common/http");
var core_2 = require("@ngx-translate/core");
var http_loader_1 = require("@ngx-translate/http-loader");
var http_2 = require("app/core/http");
var services_1 = require("app/shared/services");
var ng2_toastr_1 = require("ng2-toastr/ng2-toastr");
var animations_1 = require("@angular/platform-browser/animations");
var ng_http_loader_module_1 = require("ng-http-loader/ng-http-loader.module");
var token_interceptor_1 = require("./core/auth/token.interceptor");
var not_found_component_1 = require("./pages/not-found/not-found.component");
var change_password_component_1 = require("./account/change-password/change-password.component");
var forms_1 = require("@angular/forms");
function HttpLoaderFactory(http) {
    return new http_loader_1.TranslateHttpLoader(http);
}
exports.HttpLoaderFactory = HttpLoaderFactory;
var CustomOption = /** @class */ (function (_super) {
    __extends(CustomOption, _super);
    function CustomOption() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.animate = 'flyRight'; // you can override any options available
        _this.newestOnTop = false;
        _this.showCloseButton = true;
        _this.positionClass = 'toast-bottom-right';
        return _this;
    }
    return CustomOption;
}(ng2_toastr_1.ToastOptions));
exports.CustomOption = CustomOption;
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                platform_browser_1.BrowserModule,
                http_1.HttpClientModule,
                ng_http_loader_module_1.NgHttpLoaderModule,
                animations_1.BrowserAnimationsModule,
                app_routing_1.AppRoutingModule,
                dropdown_1.BsDropdownModule.forRoot(),
                tabs_1.TabsModule.forRoot(),
                ng2_charts_1.ChartsModule,
                core_2.TranslateModule.forRoot({
                    loader: {
                        provide: core_2.TranslateLoader,
                        useFactory: HttpLoaderFactory,
                        deps: [http_1.HttpClient]
                    }
                }),
                forms_1.ReactiveFormsModule,
                ng2_toastr_1.ToastModule.forRoot()
            ],
            declarations: [
                app_component_1.AppComponent,
                full_layout_component_1.FullLayoutComponent,
                nav_dropdown_directive_1.NAV_DROPDOWN_DIRECTIVES,
                breadcrumb_component_1.BreadcrumbsComponent,
                sidebar_directive_1.SIDEBAR_TOGGLE_DIRECTIVES,
                aside_directive_1.AsideToggleDirective,
                not_found_component_1.NotFoundComponent,
                change_password_component_1.ChangePasswordComponent
            ],
            providers: [
                {
                    provide: common_1.LocationStrategy,
                    useClass: common_1.PathLocationStrategy
                },
                {
                    provide: http_1.HTTP_INTERCEPTORS,
                    useClass: token_interceptor_1.TokenInterceptor,
                    multi: true
                },
                { provide: ng2_toastr_1.ToastOptions, useClass: CustomOption },
                http_2.BaseHttpService,
                auth_service_1.AuthService,
                auth_guard_1.AuthGuard,
                services_1.UserService,
                services_1.RoleService,
                services_1.PoliticalDivisionService,
                services_1.CommonService,
            ],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map