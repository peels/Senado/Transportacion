"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var forms_1 = require("@angular/forms");
var ng2_toastr_1 = require("ng2-toastr");
var models_1 = require("app/models");
var core_1 = require("@ngx-translate/core");
var BaseComponent = /** @class */ (function () {
    function BaseComponent(injector) {
        this.myDatePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
        };
        this.myTimePickerOptions = {
            // other options...
            dateFormat: 'dd/mm/yyyy',
        };
        this.translateService = injector.get(core_1.TranslateService);
        this.toastr = injector.get(ng2_toastr_1.ToastsManager);
    }
    BaseComponent.prototype.handleHttpError = function (error, toastr) {
        try {
            var body = JSON.parse(error.text());
            var message = '';
            if (body.length) {
                message = body.join('\n');
            }
            else if (body.messsage) {
                message = message;
            }
            else {
                message = body;
            }
            if (toastr) {
                toastr.error(message);
            }
        }
        catch (error) {
            console.log('ops! somthing go wrong on handleHttpError');
        }
    };
    BaseComponent.prototype.hasErrors = function (form, controlName) {
        return (form.controls[controlName].invalid && (form.controls[controlName].dirty || form.controls[controlName].touched));
    };
    BaseComponent.prototype.getContactInputType = function (controlType) {
        if (controlType.value === models_1.ContactType[models_1.ContactType.Email]) {
            return 'email';
        }
        return 'tel';
    };
    BaseComponent.prototype.getContactInputPlaceHolder = function (controlType) {
        if (controlType.value === models_1.ContactType[models_1.ContactType.Email]) {
            return 'email@mail.com';
        }
        return '809 5555 5555';
    };
    BaseComponent.prototype.addNewContactInformation = function (contactInformation, formBuilder) {
        contactInformation.push(this.createContactInformationGroup(new models_1.ContactInformation(), formBuilder));
    };
    BaseComponent.prototype.removeContactInformationElement = function (contactInformation, element) {
        contactInformation.removeAt(element);
    };
    BaseComponent.prototype.createContactInformationArray = function (contacts, formBuilder) {
        var _this = this;
        var contactInformation = formBuilder.array([]);
        contacts.forEach(function (contact) {
            contactInformation.push(_this.createContactInformationGroup(contact, formBuilder));
        });
        return contactInformation;
    };
    BaseComponent.prototype.createContactInformationGroup = function (contact, formBuilder) {
        return formBuilder.group({
            contactType: [contact.contactType, forms_1.Validators.required],
            contactValue: [contact.contactValue, forms_1.Validators.required],
        });
    };
    BaseComponent.prototype.ngOnDestroy = function () {
        if (this.parameterSubscription) {
            this.parameterSubscription.unsubscribe();
        }
    };
    BaseComponent.prototype.syncSelectors = function (current, target) {
        target.value = current.value;
    };
    return BaseComponent;
}());
exports.BaseComponent = BaseComponent;
//# sourceMappingURL=base-component.js.map