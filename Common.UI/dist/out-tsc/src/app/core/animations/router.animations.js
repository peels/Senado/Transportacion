"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = require("@angular/animations");
exports.routerTransition = animations_1.trigger('routerTransition', [
    animations_1.transition('* <=> *', [
        /* order */
        /* 1 */ animations_1.query(':enter, :leave', animations_1.style({ position: 'fixed', width: '100%' }), { optional: true }),
        /* 2 */ animations_1.group([
            animations_1.query(':enter', [
                animations_1.style({ transform: 'translateX(100%)' }),
                animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(0%)' }))
            ], { optional: true }),
            animations_1.query(':leave', [
                animations_1.style({ transform: 'translateX(0%)', 'z-index': -1000 }),
                animations_1.animate('0.1s ease-in-out', animations_1.style({ transform: 'translateX(-100%)', 'z-index': -1000 }))
            ], { optional: true })
        ])
    ])
]);
//# sourceMappingURL=router.animations.js.map