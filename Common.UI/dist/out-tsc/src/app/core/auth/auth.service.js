"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
require("rxjs/add/operator/toPromise");
var http_1 = require("@angular/common/http");
var AuthService = /** @class */ (function () {
    function AuthService(http, router) {
        this.http = http;
        this.router = router;
        this.uri = 'api/accounts';
    }
    AuthService_1 = AuthService;
    Object.defineProperty(AuthService, "TOKEN_KEY", {
        get: function () {
            return 'accessToken';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "isLogged", {
        get: function () {
            return !!this.currentUser;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService, "currentUser", {
        get: function () {
            return JSON.parse(sessionStorage.getItem(AuthService_1.TOKEN_KEY));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(AuthService.prototype, "currentUser", {
        get: function () {
            return JSON.parse(sessionStorage.getItem(AuthService_1.TOKEN_KEY));
        },
        enumerable: true,
        configurable: true
    });
    AuthService.prototype.logout = function () {
        var _this = this;
        var user = this.currentUser;
        if (user === null) {
            return;
        }
        var options = {
            headers: new http_1.HttpHeaders()
        };
        options.headers.append('Authorization', 'Bearer ' + user.token);
        sessionStorage.removeItem(AuthService_1.TOKEN_KEY);
        return this.http.post(this.uri + "/logout", user, options)
            .toPromise()
            .then(function (resp) {
            _this.router.navigate(['account/login']);
        });
    };
    AuthService.prototype.login = function (authRequest) {
        var headers = new http_1.HttpHeaders({ 'Content-Type': 'application/json' });
        var options = { headers: headers };
        return this.http.post(this.uri + "/login", authRequest, options)
            .toPromise()
            .then(function (resp) {
            sessionStorage.setItem(AuthService_1.TOKEN_KEY, JSON.stringify(resp));
            return resp;
        });
    };
    AuthService.prototype.validateToken = function () {
        var options = {};
        if (!options.headers) {
            options.headers = new http_1.HttpHeaders();
        }
        options.headers.append('Authorization', 'Bearer ' + this.currentUser.token);
        return this.http.get(this.uri + "/token-validation-request", options)
            .toPromise()
            .then(function (resp) { return; });
    };
    AuthService.prototype.getMenuItems = function (menus) {
        var userAccess = this.currentUser.accessList;
        menus = menus.filter(function (menu) {
            if ((menu.subMenus || []).length > 0) {
                menu.subMenus = menu.subMenus.filter(function (sub) { return userAccess.some(function (access) { return access.toLocaleLowerCase() === sub.key; }); });
            }
            return menu.subMenus.length > 0;
        });
        return menus;
    };
    AuthService.prototype.changePassword = function (currentPassword, newPassword) {
        var formData = new FormData();
        formData.append('currentPassword', currentPassword);
        formData.append('newPassword', newPassword);
        return this.http.put(this.uri + "/password", formData).toPromise()
            .then(function () { });
    };
    AuthService = AuthService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            router_1.Router])
    ], AuthService);
    return AuthService;
    var AuthService_1;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map