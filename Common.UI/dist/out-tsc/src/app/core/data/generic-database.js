"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var GenericDatabase = /** @class */ (function () {
    function GenericDatabase() {
        this.dataChange = new BehaviorSubject_1.BehaviorSubject([]);
    }
    Object.defineProperty(GenericDatabase.prototype, "data", {
        get: function () { return this.dataChange.value; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GenericDatabase.prototype, "total", {
        get: function () { return this.dataChange.value.length; },
        enumerable: true,
        configurable: true
    });
    GenericDatabase.prototype.addData = function (data) {
        this.dataChange.next(data);
    };
    return GenericDatabase;
}());
exports.GenericDatabase = GenericDatabase;
//# sourceMappingURL=generic-database.js.map