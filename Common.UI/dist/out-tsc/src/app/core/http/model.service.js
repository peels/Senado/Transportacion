"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var http_status_codes_1 = require("./http-status-codes");
var ModelService = /** @class */ (function () {
    function ModelService(http, baseUri) {
        this.http = http;
        this.baseUri = baseUri;
    }
    ModelService.prototype.handleErrorAsPromise = function (error) {
        if (error.status !== http_status_codes_1.HttpStatus.NOT_FOUND && error.status !== http_status_codes_1.HttpStatus.UNAUTHORIZED) {
            console.error(error);
        }
        return Promise.reject(error || 'Server error');
    };
    ModelService.prototype.checkDuplicatedModel = function (control) {
        var _this = this;
        if (!!this.debouncher) {
            clearTimeout(this.debouncher);
        }
        this.debouncher = setTimeout(function () {
            if (!control.value) {
                if (!!control.errors) {
                    delete control.errors.duplicated;
                }
                return;
            }
            return _this.http.get(_this.baseUri + "/" + control.value + "/code-validation")
                .then(function (data) {
                if (!!data.duplicated) {
                    control.setErrors({ duplicated: true });
                }
                else {
                    if (!!control.errors) {
                        delete control.errors.duplicated;
                    }
                }
            });
        }, 1000);
    };
    return ModelService;
}());
exports.ModelService = ModelService;
//# sourceMappingURL=model.service.js.map