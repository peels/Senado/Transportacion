"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = require("./../auth/auth.service");
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
var router_1 = require("@angular/router");
var http_status_codes_1 = require("./http-status-codes");
var http_1 = require("@angular/common/http");
var BaseHttpService = /** @class */ (function () {
    function BaseHttpService(_http, _router, _authService) {
        this._http = _http;
        this._router = _router;
        this._authService = _authService;
    }
    BaseHttpService.prototype.addAuthorizationHeaders = function (options) {
        if (!options) {
            options = {};
        }
        if (!options.headers) {
            options.headers = new Headers();
        }
        var user = this._authService.currentUser;
        options.headers.append('Authorization', 'Bearer ' + user.token);
        return options;
    };
    BaseHttpService.prototype.get = function (url, options) {
        var _this = this;
        options = this.addAuthorizationHeaders(options);
        return this._http.get(url, options)
            .toPromise()
            .catch(function (error) { return _this.handleErrorAsPromise(error, _this._authService); });
    };
    BaseHttpService.prototype.post = function (url, data, options) {
        var _this = this;
        options = this.addAuthorizationHeaders(options);
        return this._http.post(url, data, options)
            .toPromise()
            .catch(function (error) { return _this.handleErrorAsPromise(error, _this._authService); });
    };
    BaseHttpService.prototype.put = function (url, data, options) {
        var _this = this;
        options = this.addAuthorizationHeaders(options);
        return this._http.put(url, data, options)
            .toPromise()
            .catch(function (error) { return _this.handleErrorAsPromise(error, _this._authService); });
    };
    BaseHttpService.prototype.handleErrorAsPromise = function (error, authService) {
        if (error.status !== http_status_codes_1.HttpStatus.NOT_FOUND && error.status !== http_status_codes_1.HttpStatus.UNAUTHORIZED) {
            console.error(error);
        }
        if (error.status === http_status_codes_1.HttpStatus.UNAUTHORIZED) {
            // this._authService.logout();
            this._router.navigate(['/login']);
        }
        return Promise.reject(error || 'Server error');
    };
    BaseHttpService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient,
            router_1.Router,
            auth_service_1.AuthService])
    ], BaseHttpService);
    return BaseHttpService;
}());
exports.BaseHttpService = BaseHttpService;
//# sourceMappingURL=base-http.service.js.map