"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Utils;
(function (Utils) {
    function getModuleId(__moduleName) {
        return __moduleName.replace('/build', '');
    }
    Utils.getModuleId = getModuleId;
    function newGUID() {
        var d = new Date().getTime();
        if (window.performance && typeof window.performance.now === 'function') {
            d += performance.now(); // use high-precision timer if available
        }
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            // tslint:disable-next-line:no-bitwise
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            // tslint:disable-next-line:no-bitwise
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    }
    Utils.newGUID = newGUID;
    function getEnumElementsAsArray(enumParam) {
        return Object.keys(enumParam).map(function (s) { return enumParam[s]; });
    }
    function getEnumStrings(enumParam) {
        return getEnumElementsAsArray(enumParam).filter(function (s) { return typeof s === 'string'; });
    }
    Utils.getEnumStrings = getEnumStrings;
    function getEnumNumbers(enumParam) {
        return getEnumElementsAsArray(enumParam).filter(function (s) { return typeof s === 'number'; });
    }
    Utils.getEnumNumbers = getEnumNumbers;
    function firstOrDefault(collection, defaultValue) {
        for (var i in collection) {
            if (collection.hasOwnProperty(i)) {
                return collection[i];
            }
        }
        return defaultValue;
    }
    Utils.firstOrDefault = firstOrDefault;
})(Utils = exports.Utils || (exports.Utils = {}));
//# sourceMappingURL=utils.js.map