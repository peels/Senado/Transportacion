"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var transfer_panel_component_1 = require("./transfe-panel/transfer-panel.component");
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var ng2_dragula_1 = require("ng2-dragula");
var core_2 = require("@ngx-translate/core");
var TransferPanelModule = /** @class */ (function () {
    function TransferPanelModule() {
    }
    TransferPanelModule_1 = TransferPanelModule;
    TransferPanelModule.forRoot = function () {
        return {
            ngModule: TransferPanelModule_1,
        };
    };
    TransferPanelModule = TransferPanelModule_1 = __decorate([
        core_1.NgModule({
            imports: [
                common_1.CommonModule,
                ng2_dragula_1.DragulaModule,
                core_2.TranslateModule.forChild(),
            ],
            declarations: [transfer_panel_component_1.TransferPanelComponent],
            exports: [
                transfer_panel_component_1.TransferPanelComponent
            ]
        })
    ], TransferPanelModule);
    return TransferPanelModule;
    var TransferPanelModule_1;
}());
exports.TransferPanelModule = TransferPanelModule;
//# sourceMappingURL=transfer-panel.module.js.map