"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_dragula_1 = require("ng2-dragula");
var _trnasferBagId = 0;
var TransferPanelComponent = /** @class */ (function () {
    function TransferPanelComponent(dragulaService) {
        var _this = this;
        this.dragulaService = dragulaService;
        this.availableItems = [];
        this.assignedItems = [];
        this.transferBagId = "transferBag-" + _trnasferBagId++;
        dragulaService.setOptions(this.transferBagId, {
            revertOnSpill: true,
        });
        dragulaService.drag.subscribe(function (value) {
            _this.onDrag(value.slice(1));
        });
        dragulaService.drop.subscribe(function (value) {
            _this.onDrop(value.slice(1));
        });
        _trnasferBagId++;
    }
    TransferPanelComponent.prototype.ngOnInit = function () {
    };
    TransferPanelComponent.prototype.onDrag = function (args) {
        this.keepSorted();
    };
    TransferPanelComponent.prototype.onDrop = function (args) {
        this.keepSorted();
    };
    TransferPanelComponent.prototype.keepSorted = function () {
        this.assignedItems.sort(function (a, b) { return a.name - b.name; });
    };
    TransferPanelComponent.prototype.selectAvailable = function (item) {
        this.availableSelected = this.availableSelected !== item ? item : null;
    };
    TransferPanelComponent.prototype.selectAsigned = function (item) {
        this.asignedSelected = this.asignedSelected !== item ? item : null;
    };
    TransferPanelComponent.prototype.setItem = function (item) {
        if (!item) {
            return;
        }
        this.moveItem(this.availableItems, this.assignedItems, item);
    };
    TransferPanelComponent.prototype.removeItem = function (item) {
        if (!item) {
            return;
        }
        this.moveItem(this.assignedItems, this.availableItems, item);
    };
    TransferPanelComponent.prototype.setAll = function () {
        this.moveAllItems(this.availableItems, this.assignedItems);
    };
    TransferPanelComponent.prototype.removeAll = function () {
        this.moveAllItems(this.assignedItems, this.availableItems);
    };
    TransferPanelComponent.prototype.moveAllItems = function (source, target) {
        for (var i = 0; i < source.length; i++) {
            var element = source[i];
            this.moveItem(source, target, element);
            i--;
        }
    };
    TransferPanelComponent.prototype.moveItem = function (source, target, item, index) {
        index = index || source.indexOf(item);
        source.splice(index, 1);
        target.push(item);
        this.availableSelected = null;
        this.asignedSelected = null;
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], TransferPanelComponent.prototype, "availableItems", void 0);
    __decorate([
        core_1.Input(),
        __metadata("design:type", Array)
    ], TransferPanelComponent.prototype, "assignedItems", void 0);
    TransferPanelComponent = __decorate([
        core_1.Component({
            selector: 'app-transfer-panel',
            templateUrl: './transfer-panel.component.html',
            styleUrls: ['./transfer-panel.component.css']
        }),
        __metadata("design:paramtypes", [ng2_dragula_1.DragulaService])
    ], TransferPanelComponent);
    return TransferPanelComponent;
}());
exports.TransferPanelComponent = TransferPanelComponent;
//# sourceMappingURL=transfer-panel.component.js.map