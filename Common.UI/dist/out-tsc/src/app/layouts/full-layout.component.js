"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = require("./../core/auth/auth.service");
var core_1 = require("@ngx-translate/core");
var core_2 = require("@angular/core");
var services_1 = require("app/shared/services");
var spinkits_1 = require("ng-http-loader/spinkits");
var router_animations_1 = require("../core/animations/router.animations");
var FullLayoutComponent = /** @class */ (function () {
    function FullLayoutComponent(translate, authService, _commonService) {
        this.authService = authService;
        this._commonService = _commonService;
        this.disabled = false;
        this.status = { isopen: false };
        this.menuItems = [];
        this.loading = true;
        this.spinkit = spinkits_1.Spinkit;
        translate.use('es');
    }
    FullLayoutComponent.prototype.ngOnInit = function () {
        var _this = this;
        Promise.all([
            this.authService.validateToken(),
            this._commonService.getMenu()
        ]).then(function (data) {
            _this.currentUser = _this.authService.currentUser;
            _this.menuItems = _this.authService.getMenuItems(data[1]);
            _this.loading = false;
        }).catch(function (err) {
            _this.authService.logout();
        });
    };
    FullLayoutComponent.prototype.toggled = function (open) {
        console.log('Dropdown is now: ', open);
    };
    FullLayoutComponent.prototype.toggleDropdown = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    };
    FullLayoutComponent.prototype.logout = function () {
        this.authService.logout();
    };
    FullLayoutComponent.prototype.getState = function (outlet) {
        return outlet.activatedRouteData;
    };
    FullLayoutComponent = __decorate([
        core_2.Component({
            selector: 'app-dashboard',
            templateUrl: './full-layout.component.html',
            styleUrls: ['./full-layout.component.css'],
            animations: [router_animations_1.routerTransition],
        }),
        __metadata("design:paramtypes", [core_1.TranslateService,
            auth_service_1.AuthService,
            services_1.CommonService])
    ], FullLayoutComponent);
    return FullLayoutComponent;
}());
exports.FullLayoutComponent = FullLayoutComponent;
//# sourceMappingURL=full-layout.component.js.map