"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_guard_1 = require("./core/auth/auth.guard");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
// Layouts
var full_layout_component_1 = require("./layouts/full-layout.component");
var not_found_component_1 = require("./pages/not-found/not-found.component");
var change_password_component_1 = require("./account/change-password/change-password.component");
exports.routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: '',
        component: full_layout_component_1.FullLayoutComponent,
        data: {
            title: 'HomeStart'
        },
        canActivate: [auth_guard_1.AuthGuard],
        children: [
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'settings', loadChildren: './settings/settings.module#SettingsModule' },
            {
                path: 'account/change-password',
                component: change_password_component_1.ChangePasswordComponent,
                data: {
                    title: 'ChangePassword'
                },
            }
        ]
    },
    { path: 'account', loadChildren: './account/account.module#AccountModule' },
    { path: '404', component: not_found_component_1.NotFoundComponent }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        core_1.NgModule({
            imports: [router_1.RouterModule.forRoot(exports.routes)],
            exports: [router_1.RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app.routing.js.map