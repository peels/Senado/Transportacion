"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
__export(require("./services/role.service"));
__export(require("./services/user.service"));
__export(require("./services/common.service"));
__export(require("./services/political-division.service"));
//# sourceMappingURL=index.js.map