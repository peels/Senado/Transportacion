"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var testing_1 = require("@angular/core/testing");
var political_division_service_1 = require("./political-division.service");
describe('PoliticalDivisionService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [political_division_service_1.PoliticalDivisionService]
        });
    });
    it('should be created', testing_1.inject([political_division_service_1.PoliticalDivisionService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=political-division.service.spec.js.map