"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("app/core/http");
var UserService = /** @class */ (function (_super) {
    __extends(UserService, _super);
    function UserService(http) {
        return _super.call(this, http, UserService_1.baseUri) || this;
    }
    UserService_1 = UserService;
    UserService.prototype.getAll = function (count, pageNumber) {
        console.log(this.baseUri);
        return this.http.get(this.baseUri + "?count=" + count + "&pageNumber=" + pageNumber);
    };
    UserService.prototype.get = function (userName) {
        return this.http.get(this.baseUri + "/" + userName);
    };
    UserService.prototype.add = function (user) {
        return this.http.post(this.baseUri, user);
    };
    UserService.prototype.edit = function (userName, user) {
        return this.http.put(this.baseUri + "/" + userName, user);
    };
    UserService.baseUri = 'api/users';
    UserService = UserService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.BaseHttpService])
    ], UserService);
    return UserService;
    var UserService_1;
}(http_1.ModelService));
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map