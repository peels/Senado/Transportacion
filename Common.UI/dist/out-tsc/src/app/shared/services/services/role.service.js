"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var http_1 = require("app/core/http");
var core_1 = require("@angular/core");
var RoleService = /** @class */ (function (_super) {
    __extends(RoleService, _super);
    function RoleService(http) {
        return _super.call(this, http, RoleService_1.baseUri) || this;
    }
    RoleService_1 = RoleService;
    RoleService.prototype.getAll = function () {
        return this.http.get(this.baseUri)
            .catch(this.handleErrorAsPromise);
    };
    RoleService.prototype.get = function (name) {
        return this.http.get(this.baseUri + "/" + name)
            .catch(this.handleErrorAsPromise);
    };
    RoleService.prototype.add = function (role) {
        return this.http.post("" + this.baseUri, role)
            .catch(this.handleErrorAsPromise);
    };
    RoleService.prototype.edit = function (role) {
        return this.http.put(this.baseUri + "/" + role.name, role)
            .catch(this.handleErrorAsPromise);
    };
    RoleService.baseUri = 'api/roles';
    RoleService = RoleService_1 = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.BaseHttpService])
    ], RoleService);
    return RoleService;
    var RoleService_1;
}(http_1.ModelService));
exports.RoleService = RoleService;
//# sourceMappingURL=role.service.js.map