"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var services_1 = require("app/shared/services");
var RoleListComponent = /** @class */ (function () {
    function RoleListComponent(roleService) {
        this.roleService = roleService;
        this.columns = [];
        this.roles = [];
    }
    RoleListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.roleService.getAll()
            .then(function (roles) {
            _this.roles = roles;
            _this.initializeTable();
        });
    };
    RoleListComponent.prototype.initializeTable = function () {
        var self = this;
        this.columns = [
            { id: 'roleName', field: 'name', header: 'Role' },
            { id: 'Active', field: 'active', header: 'Active' },
        ];
    };
    RoleListComponent = __decorate([
        core_1.Component({
            selector: 'app-role-list',
            templateUrl: './role-list.component.html',
            styleUrls: ['./role-list.component.scss']
        }),
        __metadata("design:paramtypes", [services_1.RoleService])
    ], RoleListComponent);
    return RoleListComponent;
}());
exports.RoleListComponent = RoleListComponent;
//# sourceMappingURL=role-list.component.js.map