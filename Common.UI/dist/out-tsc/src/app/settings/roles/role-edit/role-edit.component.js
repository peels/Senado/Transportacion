"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var base_component_1 = require("../../../core/components/base-component");
var router_1 = require("@angular/router");
var models_1 = require("app/models");
var services_1 = require("app/shared/services");
var RoleEditComponent = /** @class */ (function (_super) {
    __extends(RoleEditComponent, _super);
    function RoleEditComponent(route, router, formBuilder, commonService, roleService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.route = route;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.commonService = commonService;
        _this.roleService = roleService;
        _this.injector = injector;
        _this.loading = true;
        _this.title = 'EditRol';
        _this.roleForm = new forms_1.FormGroup({});
        _this.isNew = true;
        return _this;
    }
    RoleEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.parameterSubscription = this.route.params
            .subscribe(function (params) {
            var code = params['code'];
            _this.initialize(code);
        });
    };
    RoleEditComponent.prototype.initialize = function (code) {
        var _this = this;
        Promise.all([
            this.getRole(code),
            this.commonService.getAccessList(),
        ]).then(function (data) {
            (function (role, accessList) {
                _this.rol = role;
                _this.availableAccess = accessList.filter(function (a) { return !role.access.some(function (ra) { return ra === a; }); });
                _this.initializeForm(role);
                _this.loading = false;
            }).apply(_this, data);
        });
    };
    RoleEditComponent.prototype.getRole = function (code) {
        if (code === 'new') {
            this.title = 'AddRol';
            return Promise.resolve(new models_1.Role());
        }
        this.isNew = false;
        return this.roleService.get(code);
    };
    RoleEditComponent.prototype.initializeForm = function (role) {
        var roleNameValidators = [forms_1.Validators.required];
        if (this.isNew) {
            roleNameValidators.push(this.roleService.checkDuplicatedModel.bind(this.roleService));
        }
        this.roleForm = this.formBuilder.group({
            nameDisabled: [{ value: role.name, disabled: true }],
            name: [role.name, roleNameValidators],
            active: [!!role.name ? role.active : true, forms_1.Validators.required]
        });
        this.nameControl = this.roleForm.get('name');
    };
    RoleEditComponent.prototype.onSubmit = function () {
        var _this = this;
        var rol = this.roleForm.value;
        rol.access = this.rol.access;
        var defered = null;
        if (this.isNew) {
            defered = this.roleService.add(rol);
        }
        else {
            defered = this.roleService.edit(rol);
        }
        Promise.all([
            defered,
            this.translateService.get('RolSavedMessage').toPromise()
        ]).then(function (data) {
            _this.toastr.success(data[1]);
            if (_this.isNew) {
                _this.title = 'EditRol';
                _this.router.navigate(['settings/roles/', rol.name]);
            }
        }).catch();
    };
    RoleEditComponent = __decorate([
        core_1.Component({
            selector: 'app-role-edit',
            templateUrl: './role-edit.component.html',
            styleUrls: ['./role-edit.component.scss']
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            forms_1.FormBuilder,
            services_1.CommonService,
            services_1.RoleService,
            core_1.Injector])
    ], RoleEditComponent);
    return RoleEditComponent;
}(base_component_1.BaseComponent));
exports.RoleEditComponent = RoleEditComponent;
//# sourceMappingURL=role-edit.component.js.map