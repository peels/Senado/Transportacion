"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var models_1 = require("app/models");
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var services_1 = require("app/shared/services");
var base_component_1 = require("app/core/components/base-component");
var auth_service_1 = require("../../../core/auth/auth.service");
var UserEditComponent = /** @class */ (function (_super) {
    __extends(UserEditComponent, _super);
    function UserEditComponent(_route, router, formBuilder, usersService, roleService, authService, _commonService, injector) {
        var _this = _super.call(this, injector) || this;
        _this._route = _route;
        _this.router = router;
        _this.formBuilder = formBuilder;
        _this.usersService = usersService;
        _this.roleService = roleService;
        _this._commonService = _commonService;
        _this.loading = true;
        _this.showUserType = false;
        _this.currentUser = authService.currentUser;
        return _this;
    }
    Object.defineProperty(UserEditComponent.prototype, "userType", {
        get: function () {
            return this.userForm.get('userType');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserEditComponent.prototype, "email", {
        get: function () {
            return this.userForm.get('email');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserEditComponent.prototype, "usernameControl", {
        get: function () {
            return this.userForm.get('userName');
        },
        enumerable: true,
        configurable: true
    });
    UserEditComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.parameterSubscription = this._route.params.subscribe(function (params) {
            var userName = params['userName'];
            _this.initialize(userName).then(function () {
                if (_this.isNew) {
                    var self_1 = _this;
                    setTimeout(function () {
                        self_1.userForm.controls['userName'].enable();
                    }, 300);
                }
            });
        });
    };
    UserEditComponent.prototype.initialize = function (username) {
        var _this = this;
        if (username === this.currentUser.userName) {
            history.replaceState(null, 'not-found', '404');
            this.router.navigate(['404']);
        }
        this.title = username === 'new' ? 'NewUser' : 'UserEdit';
        return Promise.all([this.getUser(username), this.roleService.getAll(), this._commonService.getUserTypesList()])
            .then(function (data) {
            (function (user, roles, userTypes) {
                _this.user = user;
                _this.roles = roles;
                console.log(_this.user);
                _this.showUserType = !_this.currentUser.userType || _this.currentUser.userType !== '*';
                _this.userTypesList = userTypes;
                _this.initializeForm(user);
                _this.loading = false;
            }).apply(_this, data);
        })
            .catch(this.handleHttpError);
    };
    UserEditComponent.prototype.getUser = function (userName) {
        if (userName === 'new') {
            this.isNew = true;
            var user = new models_1.User();
            user.active = true;
            user.userType = 0;
            return Promise.resolve(user);
        }
        this.isNew = false;
        return this.usersService.get(userName);
    };
    UserEditComponent.prototype.initializeForm = function (user) {
        var passwordValidations = this.isNew ? [forms_1.Validators.required] : [];
        var usernameValidators = [forms_1.Validators.required];
        if (this.isNew) {
            usernameValidators.push(this.usersService.checkDuplicatedModel.bind(this.roleService));
        }
        this.userForm = this.formBuilder.group({
            userName: [user.userName, usernameValidators],
            userNameDisabled: [{ value: user.userName, disabled: true }],
            password: [user.password, forms_1.Validators.compose(passwordValidations)],
            email: [user.email, forms_1.Validators.compose([forms_1.Validators.required, forms_1.Validators.email])],
            firstName: [user.firstName, forms_1.Validators.required],
            lastName: [user.lastName, forms_1.Validators.required],
            active: [!!user.firstName ? user.active : true, forms_1.Validators.required],
            roleName: [user.roleName || null, forms_1.Validators.required],
            userType: [user.userType, forms_1.Validators.required],
        });
        if (!!this.currentUser.userType) {
            this.setUserType(this.currentUser.userType);
        }
    };
    UserEditComponent.prototype.setUserType = function (userType) {
        this.userForm.patchValue({
            userType: userType,
        });
    };
    UserEditComponent.prototype.onRolChange = function () {
        var isSysAdmin = this.userForm.value.roleName === 'SystemAdministrator';
        if (isSysAdmin) {
            this.showUserType = false;
            return;
        }
        if (!this.currentUser.userType) {
            this.showUserType = true;
        }
        else {
            this.showUserType = false;
            this.setUserType(this.currentUser.userType);
        }
    };
    UserEditComponent.prototype.onSubmit = function () {
        var self = this;
        var promise;
        if (self.isNew) {
            promise = self.usersService.add(self.userForm.value);
        }
        else {
            promise = self.usersService.edit(self.user.userName, self.userForm.value);
        }
        promise
            .then(function () {
            self.translateService
                .get('UserSavedMessage')
                .toPromise()
                .then(function (message) {
                self.toastr.success(message);
                if (self.isNew) {
                    self.router.navigate(['settings/users', self.userForm.value.userName]);
                }
            });
        })
            .catch(function (error) { return self.handleHttpError(error, self.toastr); });
    };
    UserEditComponent = __decorate([
        core_1.Component({
            selector: 'app-user-edit',
            templateUrl: './user-edit.component.html',
            styleUrls: ['./user-edit.component.css'],
        }),
        __metadata("design:paramtypes", [router_1.ActivatedRoute,
            router_1.Router,
            forms_1.FormBuilder,
            services_1.UserService,
            services_1.RoleService,
            auth_service_1.AuthService,
            services_1.CommonService,
            core_1.Injector])
    ], UserEditComponent);
    return UserEditComponent;
}(base_component_1.BaseComponent));
exports.UserEditComponent = UserEditComponent;
//# sourceMappingURL=user-edit.component.js.map