"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var core_1 = require("@angular/core");
var data_1 = require("app/core/data");
var services_1 = require("app/shared/services");
var base_component_1 = require("app/core/components/base-component");
var auth_1 = require("app/core/auth");
var UserListComponent = /** @class */ (function (_super) {
    __extends(UserListComponent, _super);
    function UserListComponent(router, userService, authService, injector) {
        var _this = _super.call(this, injector) || this;
        _this.router = router;
        _this.userService = userService;
        _this.authService = authService;
        _this.users = [];
        return _this;
    }
    UserListComponent.prototype.ngOnInit = function () {
        this.fetchUsers(25, 0);
        this.initializeTable();
    };
    UserListComponent.prototype.fetchUsers = function (count, pageNumber) {
        var _this = this;
        this.userService.getAll(count, pageNumber)
            .then(function (users) {
            var currentUser = _this.authService.currentUser;
            _this.users = users.filter(function (u) { return u.userName !== currentUser.userName; });
        })
            .catch(this.handleHttpError);
    };
    UserListComponent.prototype.initializeTable = function () {
        var self = this;
        this.columns = [
            { id: 'userName', field: 'userName', header: 'UserName', type: data_1.CollumnType.Data },
            { id: 'email', field: 'email', header: 'Email', type: data_1.CollumnType.Data },
            { id: 'firstName', field: 'firstName', header: 'FirstName', type: data_1.CollumnType.Data },
            { id: 'lastName', field: 'lastName', header: 'LastName', type: data_1.CollumnType.Data },
            { id: 'roleName', field: 'roleName', header: 'Role', type: data_1.CollumnType.Data },
            { id: 'active', field: 'active', header: 'Active', type: data_1.CollumnType.Data },
        ];
    };
    UserListComponent.prototype.delete = function (user) {
        console.log("deleting useser " + user.userName);
    };
    UserListComponent.prototype.edit = function (user) {
        this.router.navigate(["settings/users/" + user.userName]);
    };
    UserListComponent.prototype.isData = function (col) {
        return col.type === data_1.CollumnType.Data;
    };
    UserListComponent = __decorate([
        core_1.Component({
            selector: 'app-user-list',
            templateUrl: './user-list.component.html',
            styleUrls: ['./user-list.component.css']
        }),
        __metadata("design:paramtypes", [router_1.Router,
            services_1.UserService,
            auth_1.AuthService,
            core_1.Injector])
    ], UserListComponent);
    return UserListComponent;
}(base_component_1.BaseComponent));
exports.UserListComponent = UserListComponent;
//# sourceMappingURL=user-list.component.js.map