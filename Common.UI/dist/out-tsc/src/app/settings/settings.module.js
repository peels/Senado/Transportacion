"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var transfer_panel_module_1 = require("./../core/transfer-panel/transfer-panel.module");
var forms_1 = require("@angular/forms");
var core_1 = require("@ngx-translate/core");
var settings_routing_1 = require("./settings.routing");
var core_2 = require("@angular/core");
var common_1 = require("@angular/common");
var user_list_component_1 = require("./users/user-list/user-list.component");
var user_edit_component_1 = require("./users/user-edit/user-edit.component");
var primeng_1 = require("primeng/primeng");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var angular_2_dropdown_multiselect_1 = require("angular-2-dropdown-multiselect");
var table_1 = require("primeng/table");
var role_list_component_1 = require("./roles/role-list/role-list.component");
var role_edit_component_1 = require("./roles/role-edit/role-edit.component");
var SettingsModule = /** @class */ (function () {
    function SettingsModule() {
    }
    SettingsModule = __decorate([
        core_2.NgModule({
            imports: [
                common_1.CommonModule,
                forms_1.FormsModule,
                forms_1.ReactiveFormsModule,
                settings_routing_1.SettingsRoutes,
                core_1.TranslateModule.forChild(),
                primeng_1.DataTableModule,
                primeng_1.SharedModule,
                ngx_bootstrap_1.TabsModule,
                transfer_panel_module_1.TransferPanelModule.forRoot(),
                primeng_1.CheckboxModule,
                ngx_bootstrap_1.CollapseModule,
                ngx_bootstrap_1.ModalModule.forRoot(),
                angular_2_dropdown_multiselect_1.MultiselectDropdownModule,
                table_1.TableModule
            ],
            declarations: [
                user_list_component_1.UserListComponent,
                user_edit_component_1.UserEditComponent,
                role_list_component_1.RoleListComponent,
                role_edit_component_1.RoleEditComponent
            ],
            providers: [
                ngx_bootstrap_1.BsModalService
            ]
        })
    ], SettingsModule);
    return SettingsModule;
}());
exports.SettingsModule = SettingsModule;
//# sourceMappingURL=settings.module.js.map