"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var user_list_component_1 = require("./users/user-list/user-list.component");
var user_edit_component_1 = require("./users/user-edit/user-edit.component");
var router_1 = require("@angular/router");
var role_edit_component_1 = require("./roles/role-edit/role-edit.component");
var role_list_component_1 = require("./roles/role-list/role-list.component");
var routes = [
    {
        path: 'users',
        data: {
            title: 'Users'
        },
        children: [
            { path: ':userName', component: user_edit_component_1.UserEditComponent },
            { path: '', component: user_list_component_1.UserListComponent }
        ]
    },
    {
        path: 'roles',
        data: {
            title: 'Roles'
        },
        children: [
            { path: ':code', component: role_edit_component_1.RoleEditComponent },
            { path: '', component: role_list_component_1.RoleListComponent }
        ]
    },
];
exports.SettingsRoutes = router_1.RouterModule.forChild(routes);
//# sourceMappingURL=settings.routing.js.map