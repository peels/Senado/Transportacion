"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@ngx-translate/core");
var ng2_toastr_1 = require("ng2-toastr");
var auth_service_1 = require("./core/auth/auth.service");
var AppComponent = /** @class */ (function () {
    function AppComponent(_translate, toastr, vcr, _authService) {
        this._translate = _translate;
        this.toastr = toastr;
        this.vcr = vcr;
        this._authService = _authService;
        this.toastr.setRootViewContainerRef(vcr);
        _translate.use('es');
        // this.toastr.success('testing');
    }
    AppComponent.prototype.ngOnDestroy = function () {
    };
    AppComponent = __decorate([
        core_1.Component({
            // tslint:disable-next-line
            selector: 'body',
            templateUrl: './app.component.html',
        }),
        __metadata("design:paramtypes", [core_2.TranslateService,
            ng2_toastr_1.ToastsManager,
            core_1.ViewContainerRef,
            auth_service_1.AuthService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map